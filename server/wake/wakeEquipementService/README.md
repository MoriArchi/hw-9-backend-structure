# Сервис тренировосного снаряжения

## Суть
Управление каталогом с доступным оборудованием. Возможность арендовать или купить товар из каталога.

Решение - NodeJS api.

## Соединение
HTTP через предоставленный API.

## Хранилище данных
Реляционная БД. К примеру - PostgreSQL.