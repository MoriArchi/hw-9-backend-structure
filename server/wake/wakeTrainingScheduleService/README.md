# Сервис планирования тренировок

## Суть
Резервирование временного интервала и аренда оборудование, если это необходимо.

Решение - NodeJA api, Google Calendar API.

## Соединение
HTTP через предоставленный API.

## Хранилище данных
Реляционная БД. К примеру - PostgreSQL.