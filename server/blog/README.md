# Blog domain

Сервис блога предоставляет API для постов и комментариев.
Только администратор имеет право публиковать новые записи в блоге. Все прошедшие проверку пользователи могут комментировать сообщения в блоге.
Сервис блога взаимодействует с сервисом маркетинга. Пользователи получают уведомления о новых сообщениях и ответах на комментарии по электронной почте или в приложении.