# Сервис создания счетов

## Суть
Создание счетов на оплату. Существует два типа счетов:
1. Пополнение баланса (баллы за наличные)
2. Оплата в баре / тренировки / аренды (баллами)

Все операции с балансом должны выполняться через создание счета-фактуры (пополнять баланс и оплачивать услугу). В случае «возврата денег» у нас должен быть способ уменьшить количество очков соответственно.

Решение - NodeJS api, абстрактный шаблон Factory для типа счета.

## Соединение
HTTP через предоставленный API.

## Хранилище данных
RabbitMQ / счета хранятся в клиентской БД.